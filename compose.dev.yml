version: "3.9"

services:
  api:
    build:
      context: . # Specifies the build context for building the API service's container image.
      dockerfile: ./docker/local/django/Dockerfile # Specifies the path to the Dockerfile for building the API service's container image.
    command: /start # Specifies the command to run when the container starts. In this case, it runs the '/start' command.
    container_name: djrsapi-api # Sets the name of the container to 'djrsapi-api'
    volumes:
      - .:/app # Mount the current directory to /app in the container
      - static_volume:/app/staticfiles # Mount static files volume
      - media_volume:/app/mediafiles # Mount media files volume
    env_file:
      - ./.envs/.local/.django # Load environment variables for Django
      - ./.envs/.local/.postgres # Load environment variables for Postgres
    depends_on:
      - postgres # Depend on the Postgres service
      - mailhog # Depend on the MailHog service
      - redis # Depend on the Redis service
    networks:
      - djrsapi-net # Connect to the djrsapi-net network
    expose:
      - "8000" # Exposes the port 8000 from the container to other linked services on the same network.

  postgres:
    build:
      context: . # Specifies the build context for building the Postgres service's container image.
      dockerfile: ./docker/local/postgres/Dockerfile # Specifies the path to the Dockerfile for building the Postgres service's container image.
    container_name: djrsapi-postgres
    volumes:
      - local_postgres_data:/var/lib/postgresql/data # Mount volume for Postgres data
      - local_postgres_data_backups:/backups # Mount volume for Postgres backups
    env_file:
      - ./.envs/.local/.postgres # Load environment variables for Postgres
    networks:
      - djrsapi-net # Connect to the djrsapi-net network

  mailhog:
    image: mailhog/mailhog:v1.0.0
    container_name: djrsapi-mailhog
    networks:
      - djrsapi-net # Connect to the djrsapi-net network
    ports:
      - "3060:8025" # Map port 8025 in the container to port 3060 on the host

  redis:
    image: redis:6-alpine
    container_name: djrsapi-redis
    networks:
      - djrsapi-net # Connect to the djrsapi-net network

  celery_worker:
    build:
      context: . # Specifies the build context for building the Celery worker service's container image.
      dockerfile: ./docker/local/django/Dockerfile # Specifies the path to the Dockerfile for building the Celery worker service's container image.
    command: /start-celeryworker
    container_name: djrsapi-celery_worker
    volumes:
      - .:/app
    env_file:
      - ./.envs/.local/.django
      - ./.envs/.local/.postgres
    depends_on:
      - redis
      - postgres
      - mailhog
    networks:
      - djrsapi-net # Connect to the djrsapi-net network

  flower:
    build:
      context: . # Specifies the build context for building the Flower service's container image.
      dockerfile: ./docker/local/django/Dockerfile # Specifies the path to the Dockerfile for building the Flower service's container image.
    command: /start-flower
    container_name: djrsapi-flower
    volumes:
      - .:/app
    env_file:
      - ./.envs/.local/.django
      - ./.envs/.local/.postgres
    ports:
      - "3070:5555"
    depends_on:
      - redis
      - postgres
    networks:
      - djrsapi-net # Connect to the djrsapi-net network

  nginx:
    build:
      context: ./docker/local/nginx # Specifies the build context for building the Nginx service's container image.
      dockerfile: Dockerfile # Specifies the path to the Dockerfile for building the Nginx service's container image.
    depends_on:
      - api
    restart: always
    volumes:
      - static_volume:/app/staticfiles
      - media_volume:/app/mediafiles
    ports:
      - "3030:80"
    networks:
      - djrsapi-net # Connect to the djrsapi-net network

networks:
  djrsapi-net:
    driver: bridge # Use the bridge driver for the djrsapi-net network

volumes:
  local_postgres_data: {} # Define an empty volume for Postgres data
  local_postgres_data_backups: {} # Define an empty volume for Postgres backups
  static_volume: # Define the static files volume
  media_volume: # Define the media files volume
