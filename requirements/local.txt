-r base.txt

psycopg2-binary==2.9.3 # A binary wrapper for the psycopg2 library used as an interface for communicating with the PostgreSQL database.
flake8==4.0.1 # A static code analysis tool for Python that combines PEP 8 (Python style guide), PyFlakes (code error checking), and McCabe (code complexity calculation).
black==22.1 # An automatic code formatting tool for Python that automatically formats the code.
isort==5.9.3 # An automatic code formatting and import sorting tool for Python.
argh==0.26.2 # A simple library for writing command-line interfaces (CLIs) in Python.
PyYAML==6.0 # A package for reading and writing YAML data in Python.
watchdog==2.1.9 # A library for monitoring file and directory changes in Python.
