"""
Custom user forms for Django admin.

These forms extend the default user management forms provided by Django admin to work with a custom User model.
"""


from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class UserChangeForm(admin_forms.UserChangeForm):
    """
    A custom form for changing user information in the Django admin interface.

    This form extends the default UserChangeForm provided by Django and uses the custom User model.
    """

    class Meta(admin_forms.UserChangeForm.Meta):
        """
        Metadata options for the UserChangeForm.

        Inherits the Meta options from the default UserChangeForm.
        """

        model = User


class UserCreationForm(admin_forms.UserCreationForm):
    """
    A custom form for creating new users in the Django admin interface.

    This form extends the default UserCreationForm provided by Django and uses the custom User model.
    """

    class Meta(admin_forms.UserCreationForm.Meta):
        """
        Metadata options for the UserCreationForm.

        Inherits the Meta options from the default UserCreationForm, and adds custom error messages for
        the username field.
        """

        model = User
        error_messages = {
            "username": {"unique": _("This username has already been taken.")}
        }
