"""
Serializers for representing the user model and creating a new user.
"""

from django.contrib.auth import get_user_model
from django_countries.serializer_fields import CountryField
from djoser.serializers import UserCreateSerializer
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for representing the user model.

    This serializer is used to represent the user model and its related profile information.
    """

    gender = serializers.CharField(source="profile.gender")
    phone_number = PhoneNumberField(source="profile.phone_number")
    profile_photo = serializers.ReadOnlyField(source="profile.profile_photo")
    country = CountryField(source="profile.country")
    city = serializers.CharField(source="profile.city")
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()

    class Meta:
        """
        Meta class for UserSerializer.

        This class defines the model and fields used by the UserSerializer.
        """

        model = User
        fields = [
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "full_name",
            "gender",
            "phone_number",
            "profile_photo",
            "country",
            "city",
        ]

    def get_first_name(self, obj):
        """
        Get the first name of the user.

        This method returns the first name of the user in title case.
        """
        return obj.first_name.title()

    def get_last_name(self, obj):
        """
        Get the last name of the user.

        This method returns the last name of the user in title case.
        """
        return obj.last_name.title()

    def get_full_name(self, obj):
        """
        Get the full name of the user.

        This method returns the full name of the user by combining the first name and last name.
        """
        return f"{obj.first_name} {obj.last_name}"

    def to_representation(self, instance):
        """
        Convert the instance to a representation.

        This method converts the instance to a representation and adds an 'admin' field for superusers.
        """
        representation = super(UserSerializer, self).to_representation(instance)
        if instance.is_superuser:
            representation["admin"] = True
        return representation


class CreateUserSerializer(UserCreateSerializer):
    """
    Serializer for creating a new user.

    This serializer is used to create a new user and is based on the UserCreateSerializer from djoser.
    """

    class Meta(UserCreateSerializer.Meta):
        """
        Meta class for CreateUserSerializer.

        This class defines the model and fields used by the CreateUserSerializer.
        """

        model = User
        fields = ["id", "username", "email", "first_name", "last_name", "password"]
