"""
This module provides a custom user manager.

The custom user manager provides methods for creating a regular user and a superuser with custom fields.
"""

from django.contrib.auth.base_user import BaseUserManager
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import gettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    Custom user manager for CustomUser model.

    This manager provides methods for creating a regular user and a superuser with custom fields.
    """

    def email_validator(self, email):
        """
        Validate the format of the email address.

        Args:
            email (str): The email address to be validated.

        Raises:
            ValueError: If the email address is not in a valid format.
        """
        try:
            validate_email(email)
        except ValidationError as exc:
            raise ValueError(_("You must provide a valid email address.")) from exc

    def create_user(
        self, username, first_name, last_name, email, password, **extra_fields
    ):
        """
        Create and save a regular user with the given username, first name, last name, email, and password.

        Args:
            username (str): The username for the user.
            first_name (str): The first name of the user.
            last_name (str): The last name of the user.
            email (str): The email address of the user.
            password (str): The password for the user.
            **extra_fields: Additional fields for the user model.

        Returns:
            CustomUser: The created user object.

        Raises:
            ValueError: If any of the required fields are missing or if the email address is not valid.
        """
        if not username:
            raise ValueError(_("Users must submit a username."))
        if not first_name:
            raise ValueError(_("Users must submit a first name."))
        if not last_name:
            raise ValueError(_("Users must submit a last name."))
        if not email:
            raise ValueError(_("Users must submit an email address."))
        if email:
            email = self.normalize_email(email)
            self.email_validator(email)
        else:
            raise ValueError(_("Base User Account: An email address is required."))
        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            **extra_fields
        )
        user.set_password(password)
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        user.save(using=self._db)
        return user

    def create_superuser(
        self, username, first_name, last_name, email, password, **extra_fields
    ):
        """
        Create and save a superuser with the given username, first name, last name, email, and password.

        Args:
            username (str): The username for the superuser.
            first_name (str): The first name of the superuser.
            last_name (str): The last name of the superuser.
            email (str): The email address of the superuser.
            password (str): The password for the superuser.
            **extra_fields: Additional fields for the user model.

        Returns:
            CustomUser: The created superuser object.

        Raises:
            ValueError: If any of the required fields are missing, if the email address is not valid,
            or if the superuser flags are not set correctly.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superusers must have is_staff=True."))

        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superusers must have is_superuser=True."))

        if not password:
            raise ValueError(_("Superuser must have a password."))

        if email:
            email = self.normalize_email(email)
            self.email_validator(email)
        else:
            raise ValueError(_("Addmin Account: An email address is required."))

        user = self.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=password,
            **extra_fields
        )

        user.save(using=self._db)

        return user
