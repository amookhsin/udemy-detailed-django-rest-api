"""
This module provides a custom user model.

The custom user model extends the Django's default user model and adds some additional fields such as first name and 
last name.
"""

import uuid

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .manager import CustomUserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model.

    This model extends the Django's default user model and adds additional fields such as first name and last name.
    """

    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    username = models.CharField(
        _("username"), db_index=True, max_length=255, unique=True
    )
    first_name = models.CharField(_("first name"), max_length=85)
    last_name = models.CharField(_("last name"), max_length=85)
    email = models.EmailField(_("email address"), db_index=True, unique=True)
    is_staff = models.BooleanField(_("staff status"), default=False)
    is_active = models.BooleanField(_("active"), default=True)

    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username", "first_name", "last_name"]

    objects = CustomUserManager()

    class Meta:
        """
        Metadata for the User model.
        """

        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self) -> str:
        return str(self.username)

    @property
    def get_full_name(self):
        """
        Return the full name of the user.

        Returns:
            str: The full name of the user.
        """
        return f"{self.first_name} {self.last_name}"

    def get_short_name(self):
        """
        Return the short name of the user.

        Returns:
            str: The short name of the user.
        """
        return self.first_name
