"""
Module containing the ProfilesConfig class for the profiles app.
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ProfilesConfig(AppConfig):
    """
    AppConfig for the profiles app.
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "core_apps.profiles"
    verbose_name = _("Profiles")

    def ready(self) -> None:
        """
        Method called when the app is ready.
        Imports signals from the profiles app.
        """
        from core_apps.profiles import signals
