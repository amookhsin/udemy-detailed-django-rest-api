import json
from rest_framework.renderers import JSONOpenAPIRenderer


class ProfileJSONRenderer(JSONOpenAPIRenderer):
    charset = "utf-8"

    def render(self, data, media_type=None, renderer_context=None):
        status_code = (
            renderer_context["response"].status_code
            if renderer_context and "response" in renderer_context
            else None
        )
        errors = data.get("errors", None)

        if errors is not None:
            return super(ProfileJsonRenderer, self)
        return json.dumps({"status_code": status_code, "profile": data})


class ProfilesJSONRenderer(JSONOpenAPIRenderer):
    charset = "utf-8"

    def render(self, data, media_type=None, renderer_context=None):
        status_code = (
            renderer_context["response"].status_code
            if renderer_context and "response" in renderer_context
            else None
        )
        errors = data.get("errors", None)

        if errors is not None:
            return super(ProfilesJsonRenderer, self)
        return json.dumps({"status_code": status_code, "profiles": data})
