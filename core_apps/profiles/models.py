"""
Models for user profiles in the application.

This module contains the Profile model, which represents user profiles in the application.
A user profile includes information such as the user's phone number, about me section, gender, country, city,
profile photo, Twitter handle, and the list of profiles that the current profile follows.

Classes:
    Profile: Represents a user profile in the application.
    Gender: Represents the gender choices for the user profile.

"""

# Importing required modules
from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from core_apps.common.models import TimeStampedUUIDModel


# Getting the user model
User = get_user_model()


# Gender class definition
class Gender(models.TextChoices):
    """
    Represents the choices for the gender field in the user profile.
    """

    MALE = "MALE", _("Male")
    FEMALE = "FEMALE", _("Female")
    UNKNOWN = "UNKNOWN", _("Unknown")


# Profile class definition
class Profile(TimeStampedUUIDModel):
    """
    Represents a user profile in the application.
    """

    # Profile fields
    user = models.OneToOneField(User, related_name="profile", on_delete=models.CASCADE)
    phone_number = PhoneNumberField(
        _("Phone Number"), max_length=11, default=None, blank=True, null=True
    )
    about_me = models.TextField(_("About Me"), default="", blank=True)
    gender = models.CharField(
        _("Gender"), choices=Gender.choices, default=Gender.UNKNOWN, max_length=15
    )
    country = CountryField(_("Country"), default="IR", blank=False, null=False)
    city = models.CharField(
        _("City"), max_length=90, default="-", blank=False, null=False
    )
    profile_photo = models.ImageField(
        _("Profile Photo"), default="/default_profile.png"
    )
    twitter_handle = models.CharField(_("Twitter Handle"), max_length=15, blank=True)
    follows = models.ManyToManyField(
        "self", symmetrical=False, related_name="followed_by", blank=True
    )

    # String representation of the profile
    def __str__(self) -> str:
        """
        Returns the string representation of the profile.
        """
        return f"{self.user.username}'s profile"  # type: ignore # pylint: disable=E1101

    # Get following list
    def following_list(self):
        """
        Returns a queryset of profiles that the current profile follows.
        """
        return self.follows.all()  # pylint: disable=E1101

    def followers_list(self):
        """
        Returns a queryset of profiles that follow the current profile.
        """
        return self.followed_by.all()

    # Follow a profile
    def follow(self, profile):
        """
        Adds the given profile to the list of profiles that the current profile follows.
        """
        self.follows.add(profile)  # pylint: disable=E1101

    # Unfollow a profile
    def unfollow(self, profile):
        """
        Removes the given profile from the list of profiles that the current profile follows.
        """
        self.follows.remove(profile)  # pylint: disable=E1101

    # Check if following a profile
    def check_following(self, profile):
        """
        Checks if the current profile is following the given profile.
        Returns True if the current profile is following the given profile, False otherwise.
        """
        return self.follows.filter(pkid=profile.pkid).exists()  # pylint: disable=E1101

    # Check if being followed by a profile
    def check_is_followed_by(self, profile):
        """
        Checks if the current profile is being followed by the given profile.
        Returns True if the current profile is being followed by the given profile, False otherwise.
        """
        return self.followed_by.filter(
            pkid=profile.pkid
        ).exists()  # pylint: disable=E1101
