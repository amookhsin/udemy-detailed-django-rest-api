"""
Module for handling signals related to user profiles.
"""

import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from api.settings.base import AUTH_USER_MODEL
from core_apps.profiles.models import Profile

logger = logging.getLogger(__name__)


@receiver(post_save, sender=AUTH_USER_MODEL)
def create_user_profile(
    sender, instance, created, **kwargs
):  # pylint: disable=unused-argument
    """
    Signal receiver function to create a user profile when a new user is created.
    """
    if created:
        Profile.objects.create(user=instance)  # pylint: disable=E1101


@receiver(post_save, sender=AUTH_USER_MODEL)
def save_user_profile(sender, instance, **kwargs):  # pylint: disable=unused-argument
    """
    Signal receiver function to save the user profile when a user is saved.
    """
    instance.profile.save()
    logger.info("%s profile created", instance)
