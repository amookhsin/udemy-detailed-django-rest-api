"""
Module containing the ProfileAdmin class for the admin interface.
"""

from django.contrib import admin
from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
    """
    Admin interface configuration for the Profile model.
    """

    list_display = ["pkid", "id", "user", "gender", "phone_number", "country", "city"]
    list_filter = ["gender", "country", "city"]
    list_display_links = ["id", "pkid"]


admin.site.register(Profile, ProfileAdmin)
