"""
This file contains custom exception handlers for handling common exceptions in a consistent way.

- `common_exception_handler`: Custom exception handler function that handles common exceptions.
- `_handle_generic_error`: Helper function to handle generic errors by formatting the response data.
- `_handle_not_found_error`: Helper function to handle not found errors by formatting the response data.
"""

from rest_framework.views import exception_handler


def common_exception_handler(exc, context):
    """
    Custom exception handler for handling common exceptions in a consistent way.

    Args:
        exc (Exception): The raised exception.
        context (dict): The context of the exception.

    Returns:
        Response: The response object.

    """
    response = exception_handler(exc, context)
    handlers = {
        "NotFound": _handle_not_found_error,
        "ValidationError": _handle_generic_error,
    }
    exception_class = exc.__class__.__name__
    if exception_class in handlers:
        return handlers[exception_class](exc, context, response)
    return response


def _handle_generic_error(exc, context, response):
    """
    Handle generic errors by formatting the response data.

    Args:
        exc (Exception): The raised exception.
        context (dict): The context of the exception.
        response (Response): The original response object.

    Returns:
        Response: The modified response object.

    """
    response.data = {"status_code": response.status_code, "errors": response.data}
    return response


def _handle_not_found_error(exc, context, response):
    """
    Handle not found errors by formatting the response data.

    Args:
        exc (Exception): The raised exception.
        context (dict): The context of the exception.
        response (Response): The original response object.

    Returns:
        Response: The modified response object.

    """
    view = context.get("view", None)

    if view and hasattr(view, "queryset") and view.queryset is not None:
        error_key = view.queryset.model._meta.verbose_name
        response.data = {
            "status_code": response.status_code,
            "errors": {error_key: response.data["detail"]},
        }
    else:
        response = _handle_generic_error(exc, context, response)
    return response
