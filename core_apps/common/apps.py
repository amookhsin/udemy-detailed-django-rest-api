"""
Module containing the CommonConfig class for the common app.
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CommonConfig(AppConfig):
    """
    AppConfig for the common app.
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core_apps.common'
    verbose_name = _('Common')
