"""
This file defines the TimeStampedUUIDModel class, an abstract base model that provides a big integer primary key,
a UUID field, and two timestamp fields.
The UUID field is automatically generated and ensures that each object is unique.
The timestamp fields are automatically set to the current date and time when an object is created or updated.
This model can be used as a parent for other models that require these fields.
"""

import uuid

from django.db import models


class TimeStampedUUIDModel(models.Model):
    """
    An abstract base model that provides a big integer primary key, a UUID field, and two timestamp fields.
    The UUID field is automatically generated and ensures that each object is unique.
    The timestamp fields are automatically set to the current date and time when an object is created or updated.
    This model can be used as a parent for other models that require these fields.
    """

    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """
        The Meta class for the TimeStampedUUIDModel model.
        This class specifies the name of the database table for this model and the default ordering for objects.
        """

        abstract = True
        ordering = ["-created_at", "-updated_at"]
