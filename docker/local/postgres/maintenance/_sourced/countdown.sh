#!/usr/bin/env bash

# Define a function called "countdown" to create a simple countdown timer
countdown() {
    # Declare a description for the function
    declare desc="A simple countdown."

    # Get the number of seconds from the input parameter
    local seconds="${1}"

    # Calculate the end time by adding the seconds to the current time
    local d=$(($(date +%s) + $seconds))

    # Loop through the countdown by comparing the end time with the current time
    while [ "$d" -ge $(date +%s) ]; do
        # Print the remaining time in hours, minutes, and seconds
        echo -ne "$(date -u --date @$(($d - $(date +%s))) +%H:%M:%S)\r"

        # Wait for a short interval before updating the countdown
        sleep 0.1
    done
}
