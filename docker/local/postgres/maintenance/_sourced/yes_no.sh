#!/usr/bin/env bash

# Define a function called "yes_no" to prompt for confirmation
yes_no() {
    # Declare a description for the function
    declare desc="Prompt for confirmation. \$\"\{1\}\" confirmation message."

    # Get the confirmation message from the input parameter
    local arg1="${1}"

    # Prompt the user for confirmation
    local response=
    read -r -p "${arg1} (y/[n])? " response

    # Check the user's response and exit with the appropriate status code
    if [[ "$response" =~ ^[Yy]$ ]]; then
        exit 0
    else
        exit 1
    fi
}
