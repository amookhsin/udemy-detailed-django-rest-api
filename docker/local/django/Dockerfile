ARG PYTHON_VERSION=3.10-slim-bullseye

FROM python:${PYTHON_VERSION} as python

####################################################
#   Create a build stage for Python dependencies   #
####################################################
FROM python as python-build-stage
ARG BUILD_ENVIRONMENT=local

# Install system dependencies required for building Python packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    libpq-dev

# Copy the Python requirements file into the /app directory
COPY ./requirements .

# Build Python wheels for the dependencies
RUN pip wheel --wheel-dir /usr/src/app/wheels \
    -r ${BUILD_ENVIRONMENT}.txt

####################################################
#  Create a run stage for the Python application   #
####################################################
FROM python AS python-run-stage

# Set build arguments and environment variables
ARG BUILD_ENVIRONMENT=local
ARG APP_HOME=/app

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV BUILD_ENV=${BUILD_ENVIRONMENT}

# Set working directory
WORKDIR ${APP_HOME}

# Install necessary system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
    libpq-dev \
    gettext \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

# Copy wheel dependencies from the build stage
COPY --from=python-build-stage /usr/src/app/wheels /wheels/

# Install Python dependencies from wheels
RUN pip install --no-cache-dir --no-index --find-links=/wheels/ /wheels/* \
    && rm -rf /wheels/

# Copy entrypoint and start scripts, and make them executable
COPY ./docker/local/django/entrypoint /entrypoint
# Remove carriage return (\r) characters from the entrypoint
RUN sed -i 's/\r$//g' /entrypoint
RUN chmod +x /entrypoint

# Copy the start script and make it executable
COPY ./docker/local/django/start /start
# Remove carriage return (\r) characters from the start
RUN sed -i 's/\r$//g' /start
RUN chmod +x /start

# Copy the start-celeryworker script and make it executable
COPY ./docker/local/django/celery/worker/start /start-celeryworker
# Remove carriage return (\r) characters from the start
RUN sed -i 's/\r$//g' /start-celeryworker
RUN chmod +x /start-celeryworker

# Copy the start-flower script and make it executable
COPY ./docker/local/django/celery/flower/start /start-flower
# Remove carriage return (\r) characters from the start
RUN sed -i 's/\r$//g' /start-flower
RUN chmod +x /start-flower

# Copy the application code into the container
COPY . ${APP_HOME}

# Set the entrypoint for the container
ENTRYPOINT [ "/entrypoint" ]
