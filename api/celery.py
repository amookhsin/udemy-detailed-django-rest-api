"""
Celery app configuration for Django project.
"""

# import necessary modules and packages
from celery import Celery
from django.conf import settings

# set default configurations for Celery app
app = Celery("api")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


# define a debug task for testing
@app.task(bind=True)
def debug_task(self):
    """
    A debug task for testing Celery app.
    """
    print(f"Request: {self.request!r}")
