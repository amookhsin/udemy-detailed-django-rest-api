from .base import *
from .base import env

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="django-insecure-c*2edf3(k928l_(3_#(t5h66l94j6k%8d#@0xnrqk#21a16d_q",
)

DEBUG = True

ALLOWED_HOSTS = ["localhost", "0.0.0.0", "127.0.0.1"]

# تنظیمات SMTP برای ارسال ایمیل
EMAIL_BACKEND = "djcelery_email.backends.CeleryEmailBackend"
EMAIL_HOST = env("EMAIL_HOST", default="mailhog")  # type: ignore
EMAIL_PORT = env("EMAIL_PORT")
DEFAULT_FROM_EMAIL = "djrsapi@email.com"
DOMAIN = env("DOMAIN")
SITE_NAME = "Authors Heven"
