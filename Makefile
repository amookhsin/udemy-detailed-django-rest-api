# Build the Docker containers using the specified configuration file and run them in detached mode, 
# removing any orphan containers and forcing recreation if necessary.
build:
	docker compose -f ./compose.dev.yml up --build -d --remove-orphans --force-recreate

# Start the Docker containers defined in the specified configuration file in detached mode.
up:
	docker compose -f ./compose.dev.yml up -d

# Stop and remove the Docker containers defined in the specified configuration file.
down:
	docker compose -f ./compose.dev.yml down

# Display the logs of the Docker containers defined in the specified configuration file.
show_logs:
	docker compose -f ./compose.dev.yml logs

# Run the database migrations for the Django application using the Docker container for the API.
migrate:
	docker compose -f ./compose.dev.yml run --rm api python3 manage.py migrate

# Create new database migrations based on the changes made to the Django models using the Docker container for the API.
makemigrations:
	docker compose -f ./compose.dev.yml run --rm api python3 manage.py makemigrations

# Show the current status of migrations for the Django application using the Docker container for the API.
showmigrations:
	docker compose -f ./compose.dev.yml run --rm api python3 manage.py showmigrations

# Collect static files from the Django application into a single location using the Docker container for the API.
collectstatic:
	docker compose -f ./compose.dev.yml run --rm api python3 manage.py collectstatic --no-inpute --clear

# Create a superuser for the Django application using the Docker container for the API.
supersuer:
	docker compose -f ./compose.dev.yml run --rm api python3 manage.py createsuperuser

# Stop and remove the Docker containers defined in the specified configuration file, including any volumes.
down-v:
	docker compose -f ./compose.dev.yml down -v

# Inspect the details of a specific Docker volume.
volume:
	docker volume inspect docker volume inspect udemy-detailed-django-rest-api_local_postgres_data

# Access the PostgreSQL database using the psql command within the Docker environment.
postgres-db:
	docker compose -f ./compose.dev.yml exec postgres psql --username=amookhsin --dbname=djrsapi

# Run the Flake8 linter within the Docker container for the API.
flake8:
	docker compose -f ./compose.dev.yml exec api flake8 .

# Check the code formatting using Black within the Docker container for the API.
black-check:
	docker compose -f ./compose.dev.yml exec api black --check --exclude=migrations .

# Display the differences in code formatting that would be made by Black within the Docker container for the API.
black-diff:
	docker compose -f ./compose.dev.yml exec api black --diff --exclude=migrations .

# Run Black code formatter within the Docker container for the API, excluding migrations
black:
	docker compose -f ./compose.dev.yml exec api black --exclude=migrations .

# Check the import sorting using isort within the Docker container for the API.
isort-check:
	docker compose -f ./compose.dev.yml exec api isort . --check-only --skip env --skip migrations

# Display the differences in import sorting that would be made by isort within the Docker container for the API.
isort-diff:
	docker compose -f ./compose.dev.yml exec api isort . --diff --skip env --skip migrations

# Run isort import sorting tool within the Docker container for the API, skipping 'env' and 'migrations' directories
isort:
	docker compose -f ./compose.dev.yml exec api isort . --skip env --skip migrations
